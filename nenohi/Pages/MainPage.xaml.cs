﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Newtonsoft.Json.Linq;

namespace nenohi.Pages
{
    /// <summary>
    /// MainPage.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainPage : UserControl
    {
        public MainPage()
        {
            InitializeComponent();
            //createRepo();
        }


        public void createRepo()
        {
            var folderDialog = new System.Windows.Forms.FolderBrowserDialog();

            if (folderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string path = folderDialog.SelectedPath;

                DirectoryInfo dirInfo = new DirectoryInfo(path + "//repoName");   //Modify
                if (dirInfo.Exists)
                {
                    // description.Text = "Already folder name exist";

                }
                else
                {
                    dirInfo.Create();

                    DirectoryInfo docDirInfo = new DirectoryInfo(dirInfo.FullName + "//Document");
                    docDirInfo.Create();

                    FileInfo infoFile = new FileInfo(dirInfo.FullName + "\\info.config");
                    //infoFile.Create();

                    JObject json = new JObject();
                    json.Add("ProjectName", "repoName.Text");
                    json.Add("Version", "1.0.0");

                    File.WriteAllText(infoFile.FullName, json.ToString());

                    moveToRepo("repoName.Text");
                }
            }

        }

        private void moveToRepo(string repoName)
        {

        }
    }
}
